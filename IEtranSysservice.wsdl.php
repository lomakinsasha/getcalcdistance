<?php
/**
 * IEtranSysservice.wsdl.php
 */
header("Content-Type: text/xml; charset=utf-8");
echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
?>
<definitions xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"
             xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/"
             xmlns:tns="http://<?=$_SERVER['HTTP_HOST']?>/"
             xmlns:xs="http://www.w3.org/2001/XMLSchema"
             xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/"
             xmlns:http="http://schemas.xmlsoap.org/wsdl/http/"
             name="SmsWsdl"
             xmlns="http://schemas.xmlsoap.org/wsdl/">
    <types>
        <xs:schema elementFormDefault="qualified"
                   xmlns:tns="http://schemas.xmlsoap.org/wsdl/"
                   xmlns:xs="http://www.w3.org/2001/XMLSchema"
                   targetNamespace="http://<?=$_SERVER['HTTP_HOST']?>/">
            <xs:complexType name="GetCalcDistance" mixed="true">
                <xs:sequence>
                    <xs:element name="detailLevel" type="xs:decimal" default="2" />
                    <xs:element name="distType" type="xs:decimal" default="1" />
                    <xs:element name="openCH" type="xs:decimal" default="0" />
                    <xs:element name="addBoard" type="xs:decimal" default="0" />
                    <xs:element name="useTarif" type="xs:decimal" default="0" />
                    <xs:element name="Distance">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element name="distStationCode" type="xs:string" minOccurs="2" maxOccurs="unbounded" />  
                            </xs:sequence>           
                        </xs:complexType>
                    </xs:element>
                </xs:sequence>
            </xs:complexType>
            <xs:complexType name="Distance" mixed="true">
                <xs:sequence>
                    <xs:element name="distStationCode" type="xs:string" />
                    <xs:element name="distStationName" type="xs:string" />
                    <xs:element name="distTrackTypeID" type="xs:decimal" default="1" />
                    <xs:element name="distTrackTypeName" type="xs:string" />
                    <xs:element name="distTranspTypeID" type="xs:decimal" />
                    <xs:element name="distTranspTypeName" type="xs:string" />
                    <xs:element name="distMinWay" type="xs:decimal" />
                    <xs:element name="distSign" type="xs:string" />
                </xs:sequence>
            </xs:complexType>
            <xs:element name="Request">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="getCalcDistance" type="GetCalcDistance" />
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
            <xs:element name="Response">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="distStationCode" type="xs:string" />
                       <!--  <xs:element name="distStationName" type="xs:string" />
                        <xs:element name="distTrackTypeID" type="xs:decimal" default="1" />
                        <xs:element name="distTrackTypeName" type="xs:string" />
                        <xs:element name="distTranspTypeID" type="xs:decimal" />
                        <xs:element name="distTranspTypeName" type="xs:string" />
                        <xs:element name="distMinWay" type="xs:decimal" />
                        <xs:element name="distSign" type="xs:string" /> -->
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
        </xs:schema>
    </types>

    <!-- Сообщения процедуры sendSms -->
    <message name="getCalcDistanceRequest">
        <part name="Request" element="tns:Request" />
    </message>
    <message name="getCalcDistanceResponse">
        <part name="Response" element="tns:Response" />
    </message>

    <!-- Привязка процедуры к сообщениям -->
    <portType name="IEtranSysservicePortType">
        <operation name="getCalcDistance">
            <input message="tns:getCalcDistanceRequest" />
            <output message="tns:getCalcDistanceResponse" />
        </operation>
    </portType>

    <!-- Формат процедур веб-сервиса -->
    <binding name="getCalcDistanceBinding" type="tns:IEtranSysservicePortType">
        <soap:binding transport="http://schemas.xmlsoap.org/soap/http" />
        <operation name="getCalcDistance">
            <soap:operation soapAction="" />
            <input>
                <soap:body use="literal" />
            </input>
            <output>
                <soap:body use="literal" />
            </output>
        </operation>
    </binding>

    <!-- Определение сервиса -->
    <service name="GetCalcDistanceBinding">
        <port name="GetCalcDistancePort" binding="tns:getCalcDistanceBinding">
            <soap:address location="http://<?=$_SERVER['HTTP_HOST']?>/soap/rzd/getCalcDistanceService.php" />
        </port>
    </service>
</definitions>
